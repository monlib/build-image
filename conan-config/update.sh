#!/bin/bash

set -e

conan remote add monlib https://api.bintray.com/conan/monlib/Monlib
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
cp -r /conan-config/data/* "$CONAN_USER_HOME"/.conan
conan profile new --detect default

