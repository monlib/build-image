# Build Images

This repo contains dockerfiles to create images for CI/CD across the monlib
group.

Dockerfiles are partially based off of
[conan-io/conan-docker-tools](https://github.com/conan-io/conan-docker-tools),
created by  JFrog LTD and licensed under the MIT License.

## Building

```
conan build -f gcc73/Dockerfile .
```

Replace `gcc73` with the image's directory.
